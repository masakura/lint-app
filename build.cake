#tool JetBrains.ReSharper.CommandLineTools&version=2020.1.0
#tool ReSharperReports&version=0.4.0
#addin Cake.ReSharperReports&version=0.11.1

var target = Argument("target", "Lint");

var solution = new FilePath("./LintApp.sln");

Task("Lint")
    .Does(() => InspectCode("./LintApp.sln", new InspectCodeSettings {
        OutputFile = "reports/lint.xml",
        ThrowExceptionOnFindingViolations = true,
    }))
    .Finally(() => ReSharperReports("reports/lint.xml", "reports/lint.html"));

RunTarget(target);